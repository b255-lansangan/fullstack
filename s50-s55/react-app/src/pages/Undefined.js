import Banner from '../components/Banner';
import { Link } from 'react-router-dom';

export default function Undefined() {
  const data = {
    title: "Page Not Found",
    destination: "/",
    content: "Go back to the",
    label: "homepage"

  };

  return (
    <Banner data={data}>
    </Banner>
  );
}
