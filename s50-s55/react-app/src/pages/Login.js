import {useState, useEffect, useContext } from 'react'
import {Form, Button} from 'react-bootstrap'
import { Row, Col , Card} from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'

export default function Login(props) {

  // Allows us to consume the User context object and it's properties to use for user validation
  const { user, setUser} = useContext(UserContext);

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  // state to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState('')
  console.log(email)
  console.log(password)

  
  function authenticate (e) {
    // prevents page redirection via form submission
    e.preventDefault()


    // Process a fetch request to the corresponding backend API
    // The header information "Content-Type" is used to specify that the information being sent to the backend will be sent in the form of JSON
    // The fetch request will communicate with our backend application providing it with a stringified JSON
    fetch('http://localhost:4000/users/login', {
      method: 'POST',
      headers: {
        'Context-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      // If no user information is found, the "access" property will not be available and will return undefined
       if(typeof data.access !== "undefined"){
        localStorage.setItem('token', data.access);
        retrieveUserDetails(data.access);

        Swal.fire({
          title: "Login Successful",
          icon: "success",
          text: "Welcome to Zuitt"
        });
      } else {
        Swal.fire({
          title: "Authentication failed",
          icon: "error",
          text: "Check your login details and try again"
        })
      }
    })

    // Set the email of the authenticated user in the local storage
    // Syntax
    // localStorage.setItem('email', email)

    // Set the global user stat to have properties obtained  from local storage
    // Though access to the user information can be done via the localStorage this is necessary to update the user state which will help update the App component and rerender it to avoid refreshing the page
    // When state change component are rerendered and the AppNavBar component will be updated based on the user credentials
    // setUser({
    //   email: localStorage.getItem('email')
    // });


    // clear input fields
    setEmail('')
    setPassword('')

    alert(`${email} You have successfully login`)
  }

  const retrieveUserDetails = (token) => {


    // The token will be sent as part of the request header information
    fetch('http://localhost:4000/users/details', {
      headers: {
        Authorization: `Bearer ${ token }`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })
    })
  }


  useEffect(() => {
    // validation to enable submit button when all fields are populated and both passwords match
    if(email !== '' && password !== '') {
      setIsActive(true)
    } else {
      setIsActive(false)
    }
  }, [email, password])


  return (
  (user.id !== null) ?
    <Navigate to="/courses"/>
    :
    <Row className="mt-3 mb-3">
    <Col xs={12} md={4}>
      <Card className="cardCourseCard p-3">
          <h2>Login</h2>
          <Form onSubmit={(e) => authenticate(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password1" className="mb-2">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

        

            {isActive ? 
              <Button variant="success" type="submit" id="submitBtn">
                  Submit
              </Button>
              :
              <Button variant="success" type="submit" id="submitBtn" disabled>
                  Submit
              </Button>
            }
        </Form>

      </Card>
    </Col>
    </Row>
  )
}