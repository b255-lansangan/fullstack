import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


const data = {
	title: "Zuitt Coding Bootcamp",
	content: "Opportunities for everyone, everywhere",
	destination: "/courses",
	label: "Enroll now!"
}


export default function Home(){

	return (
		<>
			<Banner data={data}/>
    		<Highlights/>
		</>
	)
}
