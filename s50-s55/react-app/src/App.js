import { Fragment } from 'react'
import { Container } from 'react-bootstrap'
import { BrowserRouter as Router } from 'react-router-dom'
import { Route, Routes } from 'react-router-dom'
import Home from './pages/Home'
import Courses from './pages/Courses'
import CourseView from './components/CourseView'
import AppNavBar from './components/AppNavBar'
import Register from './pages/Register'
// import Banner from './components/Banner'
// import Highlights from './components/Highlights'
import Login from "./pages/Login" 
import Logout from "./pages/Logout" 
import Undefined from "./pages/Undefined" 
import React from 'react'
import { useState, useEffect } from 'react';
import './App.css';
import { UserProvider } from './UserContext';

function App() {

  // State hook for the user state that's defined here for a global scope
  // Initialized as an object with properties from the local storage
  // This will be used to store the user information and will be used for validating if a user is logged in on the app or not

  const [user, setUser] = useState({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  });

  // Function for clearing storage on logout

  const unsetUser = () => {
    localStorage.clear()
  }

  // Used to check if the user information is properly stored upon login
  useEffect(() => {
    console.log(user);
    console.log(localStorage)
  }, [user])

  return (
   <UserProvider value = {{user, setUser, unsetUser}}>
    <Router>
      <React.Fragment>
        <Container fluid>
          <AppNavBar />
          <Routes>
              <Route path="/" element={<Home/>} />
              <Route path="/courses" element={<Courses/>} />
              <Route path="/courseView/:courseId" element={<CourseView/>} />
              <Route path="/register" element={<Register/>} />
              <Route path="/login" element={<Login/>} />
              <Route path="/logout" element={<Logout/>} />
              <Route path="*" element={<Undefined />} />
          </Routes>
        </Container>
      </React.Fragment>
     </Router> 
    </UserProvider> 
  );
}

export default App;


// Create route paths to the Course component, Register component, login component
// Route names should be /courses, /register, /login