/*import Button from 'react-bootstrap/Button';
// Bootstrap grid system components
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
*/

/*
import Button from 'react-bootstrap/Button';
// Bootstrap grid system components
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';*/
import { Link } from 'react-router-dom';
import { Button, Row, Col} from 'react-bootstrap'

export default function Banner({data}){

	const { title, content, destination, label } = data;
	return(

		<Row>
			<Col>
				<h1>{title}</h1>
				<p>{content}</p>
				{/*<Button variant="primary" as={Link} to={destination}>{label}</Button>*/}
				<Button variant="primary" as={Link} to={destination}>{label}</Button>
			</Col>
		</Row>

	)

}