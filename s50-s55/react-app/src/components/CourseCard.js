import { Row, Col , Card, Button} from 'react-bootstrap';
import { useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}){
	
	// Check to see if the data was successfully passsed
	/*console.log(props);
	// Every component receives information in a form of an object
	console.log(typeof props);*/

	// Use the state hook for this component to be able to store its state
	// States are used to keep track of information related to individual componnets
	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(30);
	// Using the state hook returns an arrray with the first element being a value and the second element as a function that used to change the value of the first element
	console.log(useState(0));

	// Function that keeps track of the enrollee for a course
	// By default, JavaScript is a synchronous it executes code from the top of the file all the way down
	// The setter function for useStates are asynchronous allowing it to executes seperately from other code in the program
	// The "setCount" function is being executed while the "console.log" is already completed

/*	function enroll(){
		if (count === 30 || seats === 0) { 
			alert('No more seats'); 
			} else {
			   setCount(count + 1);
			   console.log('Enrollees: ' + count); 
			   setSeats(seats - 1);
			 }

	useEffect(() =>{
	 if (seats === 0){
		setIsOpen(false);
	 }
	}, [seats])
	  	
}*/


	const { name, description, price, _id } = courseProp;
	return(
			<Row className="mt-3 mb-3">
		        <Col xs={12} md={12}>
		            <Card className="cardCourseCard p-3">
		                <Card.Body>
		                    <Card.Title>
		                        {name}
		                    </Card.Title>
		                    <Card.Subtitle>
		                       Description:
		                    </Card.Subtitle>
		                    <Card.Text>
		                        {description}
		                    </Card.Text>
		                    <Card.Subtitle>
		                        Price:
		                    </Card.Subtitle>
		                    <Card.Text>
		                        PhP {price}
		                    </Card.Text>
		                    <Card.Text>
		                        Enrollee: {count}
		                    </Card.Text>
{/*		                    <Button className="bg-primary" onClick={enroll}>Enroll</Button>
*/}		                    <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
		                </Card.Body>
		            </Card>
		        </Col>
		        
			</Row>
		)
}

CourseCard.propTypes = {
	// The shape method is use to check if a prop object confirms to a specific shape
	course: PropTypes.shape({
		// Define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,

	})
}